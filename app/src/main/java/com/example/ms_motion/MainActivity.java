package com.example.ms_motion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import edu.umd.cmsc436.sheets.Sheets;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Sheets.Host{


    private static final int LIB_ACCOUNT_NAME_REQUEST_CODE = 1001;
    private static final int LIB_AUTHORIZATION_REQUEST_CODE = 1002;
    private static final int LIB_PERMISSION_REQUEST_CODE = 1003;
    private static final int LIB_PLAY_SERVICES_REQUEST_CODE = 1004;

    private Button left_hand;
    private Button right_hand;
    private Button left_foot;
    private Button right_foot;

    private Sheets sheet;

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Motion Test");

        left_hand = (Button) findViewById(R.id.left_hand);
        right_hand = (Button) findViewById(R.id.right_hand);
        left_foot = (Button) findViewById(R.id.left_foot);
        right_foot = (Button) findViewById(R.id.right_foot);

        left_hand.setOnClickListener(this);
        right_hand.setOnClickListener(this);
        left_foot.setOnClickListener(this);
        right_foot.setOnClickListener(this);
        sheet = new Sheets(this, getString(R.string.app_name), getString(R.string.sheet));
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        int count = 0;
        Log.d("reset", String.valueOf(settings.getBoolean("reset", false)));
        if (settings.getBoolean("reset", false)) {
            reset();
        }
        if (settings.getBoolean("left_hand", false)) {
            left_hand.setEnabled(false);
            count++;
        }
        if (settings.getBoolean("right_hand", false)) {
            right_hand.setEnabled(false);
            count++;
        }
        if (settings.getBoolean("left_foot", false)) {
            left_foot.setEnabled(false);
            count++;
        }
        if (settings.getBoolean("right_foot", false)) {
            right_foot.setEnabled(false);
            count++;
        }
        if (count == 4) {
            reset();
            displayAlert("You have completed all your trials! Tap the screen to restart.");
        }

    }

    public void reset() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("left_hand", false);
        editor.putBoolean("right_hand", false);
        editor.putBoolean("left_foot", false);
        editor.putBoolean("right_foot", false);
        left_hand.setEnabled(true);
        right_hand.setEnabled(true);
        left_foot.setEnabled(true);
        right_foot.setEnabled(true);
        editor.putBoolean("reset", false);
        editor.commit();
    }

    private void startButtonClick(int limb) {
        Intent intent = new Intent(this, Calibrate.class);
        intent.putExtra("limb", limb);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.left_hand:
                startButtonClick(0);
                break;
            case R.id.right_hand:
                startButtonClick(1);
                break;
            case R.id.left_foot:
                startButtonClick(2);
                break;
            case R.id.right_foot:
                startButtonClick(3);
                break;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                settingsButtonClick();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void settingsButtonClick() {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    private void displayAlert(String text){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(text+"\n");
        builder1.setCancelable(true);
        builder1.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        sheet.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getRequestCode(Sheets.Action action) {
        switch (action) {
            case REQUEST_ACCOUNT_NAME:
                return LIB_ACCOUNT_NAME_REQUEST_CODE;
            case REQUEST_AUTHORIZATION:
                return LIB_AUTHORIZATION_REQUEST_CODE;
            case REQUEST_PERMISSIONS:
                return LIB_PERMISSION_REQUEST_CODE;
            case REQUEST_PLAY_SERVICES:
                return LIB_PLAY_SERVICES_REQUEST_CODE;
            default:
                return -1; // boo java doesn't know we exhausted the enum
        }
    }

    @Override
    public void notifyFinished(Exception e) {
        if (e != null) {
            throw new RuntimeException(e);
        }
        Log.i(getClass().getSimpleName(), "Done");
    }
}

package com.example.ms_motion;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import java.util.Arrays;

public class Calibrate extends AppCompatActivity implements SensorEventListener{

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor rotation;
    private Sensor linearSensor;

    private Vibrator vibrate;

    private TextView orientation;
    private TextView diff;
    private TextView calib_instructions;

    //deviation allowed in the rotation
    private double error;
    //action==0 means application just started, action==1 means waiting for a tap to calibrate
    private int action;
    private int calibrate = 3;
    private double x;
    private double y;
    private double z;
    //position 0 means extended position 1 means shoulder position
    private int position;
    private double[] gravity = new double[3];
    private double[] linear_acceleration = new double[3];
    private double[] x_vals = new double[3];
    private double[] y_vals = new double[3];
    private double[] z_vals = new double[3];
    private double[] x2_vals = new double[3];
    private double[] y2_vals = new double[3];
    private double[] z2_vals = new double[3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibrate);
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        rotation = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        linearSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        vibrate = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        calib_instructions = (TextView) findViewById(R.id.calib_instructions);
        action = 0;
        position = 0;

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 1) {
            switch(action) {
                case 0: calibrate();
                    break;
                case 1: checkMovement();
                    break;
                case 2: motionStart();
                    break;
                default: break;
            }
        }
        return super.onTouchEvent(event);
    }

    public void motionStart() {
        Intent intent = new Intent(this, Motion.class);
        intent.putExtra("x", x_vals);
        intent.putExtra("y", y_vals);
        intent.putExtra("z", z_vals);
        intent.putExtra("x2", x2_vals);
        intent.putExtra("y2", y2_vals);
        intent.putExtra("z2", z2_vals);
        intent.putExtra("limb", getIntent().getExtras().getInt("limb"));
        startActivity(intent);
    }

    public boolean checkMovement() {
//        double vector = Math.sqrt(x*x + y*y + z*z);
//      Test to only get values when phone is still
//        if (vector > 10 || vector < 9.6) {
//            calib_instructions.setText("please hold the phone steady try again");
//            return false;
//        } else {
//            action = 2;
//            if (position == 0) {
//                x_vals[calibrate-1] = x;
//                y_vals[calibrate-1] = y;
//                z_vals[calibrate-1] = z;
//            } else {
//                x2_vals[calibrate-1] = x;
//                y2_vals[calibrate-1] = y;
//                z2_vals[calibrate-1] = z;
//                Log.d("x2vals", Arrays.toString(x2_vals));
//                calibrate--;
//            }
//            position = position ^ 1;
//            calibrate();
//        }
        action = 2;
        if (position == 0) {
            x_vals[calibrate-1] = x;
            y_vals[calibrate-1] = y;
            z_vals[calibrate-1] = z;
            Log.d("xvals", Arrays.toString(x_vals));
            Log.d("yvals", Arrays.toString(y_vals));
            Log.d("zvals", Arrays.toString(z_vals));
        } else {
            x2_vals[calibrate-1] = x;
            y2_vals[calibrate-1] = y;
            z2_vals[calibrate-1] = z;
            Log.d("x2vals", Arrays.toString(x2_vals));
            Log.d("y2vals", Arrays.toString(y2_vals));
            Log.d("z2vals", Arrays.toString(z2_vals));
            calibrate--;
        }
        position = position ^ 1;
        calibrate();
        return true;
    }

    public void calibrate() {
        if (position == 0) {
            vibrate.vibrate(100);
            calib_instructions.setText("Please extend your arm, hold the phone steady, and tap the screen to calibrate");
            action = 1;
        } else {
            vibrate.vibrate(100);
            calib_instructions.setText("Please touch your phone to your shoulder, hold the phone steady, and tap the screen to calibrate");
            action = 1;
        }
        if (calibrate == 0) {
            vibrate.vibrate(200);
            if (checkError()) {
                calib_instructions.setText("You are done! Please tap the screen when you are ready to start the test");
                action = 2;
                position = 2;
            } else  {
                calib_instructions.setText("There was too much deviance in your calibration, please tap the screen to try again");
                action = 0;
                position = 0;
                calibrate = 3;
            }

            return;
        }
    }

    public boolean checkError() {
        if (determineRange(x_vals) && determineRange(y_vals) && determineRange(z_vals)
                && determineRange(x2_vals) && determineRange(y2_vals) && determineRange(z2_vals)) {
            return true;
        }
        return false;

    }
    public boolean determineRange(double[] arr) {
        double min = arr[0];
        double max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            } else if (arr[i] > max) {
                max = arr[i];
            }
        }
        Log.d("min", String.valueOf(min));
        Log.d("max", String.valueOf(max));
        Log.d("error", String.valueOf(error));
        return Math.abs(min - max) <= error;
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String calibration = settings.getString("calibration", ".2");
        if (calibration.isEmpty()) {
            calibration = ".2";
        }
        error = Double.parseDouble(calibration);
        Log.d("error", String.valueOf(error));

        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, linearSensor, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, rotation, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
//        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//            final double alpha = 0.8;
//            gravity[0] = alpha * gravity[0] + (1-alpha) * event.values[0];
//            gravity[1] = alpha * gravity[1] + (1-alpha) * event.values[1];
//            gravity[2] = alpha * gravity[2] + (1-alpha) * event.values[2];
//
//            linear_acceleration[0] = event.values[0] - gravity[0];
//            linear_acceleration[1] = event.values[1] - gravity[1];
//            linear_acceleration[2] = event.values[2] - gravity[2];
//
//            x = event.values[0];
//            y = event.values[1];
//            z = event.values[2];
//        }
//        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
//            test[0] = event.values[0];
//            test[1] = event.values[1];
//            test[2] = event.values[2];
//        }
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}

package com.example.ms_motion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import edu.umd.cmsc436.sheets.Sheets;

public class Motion extends AppCompatActivity implements View.OnClickListener, SensorEventListener, Sheets.Host {

    private static final int LIB_ACCOUNT_NAME_REQUEST_CODE = 1001;
    private static final int LIB_AUTHORIZATION_REQUEST_CODE = 1002;
    private static final int LIB_PERMISSION_REQUEST_CODE = 1003;
    private static final int LIB_PLAY_SERVICES_REQUEST_CODE = 1004;

    Sheets sheet;
    private String id;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor rotation;
    private Sensor linearSensor;

    private SharedPreferences settings;
    private SharedPreferences.Editor editor;

    private Vibrator vibrate;

    private Button start;
    private TextView display;
    private TextView title;

    private int limb;
    private int start_time = 3;
    private int time = 10;
    //total count for all trials
    private float total = 0;
    //temp count per trial
    private int count = 0;
    //action = 0 means extend, action = 1 means to retract
    private int action = 0;
    //margin of error for rotation
    private double margin;
    private int trials;
    private int tempTrials;
    private boolean started;

    private double[] x;
    private double[] y;
    private double[] z;
    private double[] x2;
    private double[] y2;
    private double[] z2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        editor = settings.edit();

        started = false;

        limb = getIntent().getExtras().getInt("limb");

        String str = settings.getString("error", ".05");
        if (str.isEmpty()) {
            str = ".05";
        }
        margin = Double.parseDouble(str);
        Log.d("margin", String.valueOf(margin));

        str = settings.getString("trials", "3");
        if (str.isEmpty()) {
            str = "3";
        }
        trials = Integer.parseInt(str);
        Log.d("trials", String.valueOf(trials));

        tempTrials = trials;
        str = settings.getString("name", "t03p02");
        if (str.isEmpty()) {
            str = "t03p02";
        }
        id = str;

        x = getRange(getIntent().getExtras().getDoubleArray("x"));
        y = getRange(getIntent().getExtras().getDoubleArray("y"));
        z = getRange(getIntent().getExtras().getDoubleArray("z"));
        x2 = getRange(getIntent().getExtras().getDoubleArray("x2"));
        y2 = getRange(getIntent().getExtras().getDoubleArray("y2"));
        z2 = getRange(getIntent().getExtras().getDoubleArray("z2"));

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        rotation = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        linearSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        vibrate = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        start = (Button) findViewById(R.id.start_button);
        title = (TextView) findViewById(R.id.title);
        display = (TextView) findViewById(R.id.display);
        display.setVisibility(View.INVISIBLE);
        start.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, linearSensor, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, rotation, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    public double[] getRange(double[] arr) {
        double min = arr[0];
        double max = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            } else if (arr[i] > max) {
                max = arr[i];
            }
        }
        double[] res = {min-margin, max + margin};
        return res;
    }


    public void runGame() {
        Runnable task = new Runnable() {

            @Override
            public void run() {
                if (time == 0) {
                    tempTrials --;
                    started = false;
                    display.setVisibility(View.INVISIBLE);
                    if (tempTrials != 0) {
                        action = 0;
                        title.setText("Score: " + String.valueOf(count));
                        vibrate.vibrate(200);
                        start.setEnabled(true);
                        start.setVisibility(View.VISIBLE);
                        start.setText("Tap the screen to begin your next trial");
                        start_time = 3;
                        time = 10;
                        count = 0;
                    } else {
                        action = 2;
                        title.setTextSize(30);
                        title.setText("Score: " + String.valueOf(count) + " Total Score: " + String.valueOf(total));
                        vibrate.vibrate(200);
                        start.setVisibility(View.VISIBLE);
                        start.setText("Tap the screen to return to the main screen");
                        savePreferences();
                    }
                } else {
                    started = true;
                    title.setText(String.valueOf(time) + " seconds");
                    time -= 1;
                    final Handler h = new Handler();
                    h.postDelayed(this, 1000);
                }
            }
        };
        task.run();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 1 && action == 2) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        return super.onTouchEvent(event);
    }


    public void savePreferences() {
        sheet = new Sheets(this, getString(R.string.app_name), getString(R.string.sheet));
        switch(limb) {
            case 0: editor.putBoolean("left_hand", true);
                break;
            case 1: editor.putBoolean("right_hand", true);
                break;
            case 2: editor.putBoolean("left_foot", true);
                break;
            case 3: editor.putBoolean("right_foot", true);
                break;
        }
        if (limb == 0) {
            float res = total/(float)trials;
            Log.d("res", String.valueOf(total));
            Log.d("res", String.valueOf(trials));
            Log.d("res", String.valueOf(res));;

            sheet.writeData(Sheets.TestType.LH_CURL, id, total/trials);
        }
        if (limb == 1) {
            sheet.writeData(Sheets.TestType.RH_CURL, id, total/trials);
        }
        editor.commit();
    }

    @Override
    public void onClick(View v) {
        if (action != 2) {
            switch(v.getId()) {
                case R.id.start_button:
                    startButtonClick();
                    break;
            }
        }
    }

    private void startButtonClick() {
        display.setVisibility(View.VISIBLE);
        display.setText("Extend");
        Runnable task = new Runnable() {
            @Override
            public void run() {
                if (start_time != 0) {
                    start.setText("Beginning in " + String.valueOf(start_time) + " seconds");
                    start_time -= 1;
                    final Handler h = new Handler();
                    h.postDelayed(this, 1000);
                } else {
                    start.setEnabled(false);
                    start.setVisibility(View.INVISIBLE);
                    runGame();
                }
            }
        };

        task.run();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (started && event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {

            if (action == 0 && event.values[0] <= x[1] && event.values[0] >= x[0]
                    && event.values[1] <= y[1] && event.values[1] >= y[0]
                    && event.values[2] <= z[1] && event.values[2] >= z[0]) {
                vibrate.vibrate(50);
                display.setText("Retract");
                action = action ^ 1;
            }
            else if (action == 1 && event.values[0] <= x2[1] && event.values[0] >= x2[0]
                    && event.values[1] <= y2[1] && event.values[1] >= y2[0]
                    && event.values[2] <= z2[1] && event.values[2] >= z2[0]) {
                count++;
                total++;
                vibrate.vibrate(50);
                display.setText("Extend");
                action = action ^ 1;
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        sheet.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        sheet.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public int getRequestCode(Sheets.Action action) {
        switch (action) {
            case REQUEST_ACCOUNT_NAME:
                return LIB_ACCOUNT_NAME_REQUEST_CODE;
            case REQUEST_AUTHORIZATION:
                return LIB_AUTHORIZATION_REQUEST_CODE;
            case REQUEST_PERMISSIONS:
                return LIB_PERMISSION_REQUEST_CODE;
            case REQUEST_PLAY_SERVICES:
                return LIB_PLAY_SERVICES_REQUEST_CODE;
            default:
                return -1; // boo java doesn't know we exhausted the enum
        }
    }

    @Override
    public void notifyFinished(Exception e) {
        if (e != null) {
            throw new RuntimeException(e);
        }
        Log.i(getClass().getSimpleName(), "Done");
    }
}
